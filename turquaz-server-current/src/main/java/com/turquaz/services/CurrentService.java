package com.turquaz.services;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.hibernate.Session;
import org.oasisopen.sca.annotation.ManagedTransaction;

import com.turquaz.model.CurrentCard;
import com.turquaz.model.MenuItem;

@Path("")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
@ManagedTransaction
public class CurrentService {

    private Session session;
	
    @PersistenceContext(name = "messageEmf", unitName = "message")
    public void setSession(Session session) {
        this.session = session;
    }

    @POST
    @Path("/save")
    public Response saveCurrent(CurrentCard currentCard) {
        session.save(currentCard);
        return Response.created(URI.create(currentCard.getId().toString())).build();
    }

    
    @GET
    @Path("/getMenu")
    public List<MenuItem> getMainMenu() {
    	
    	List<MenuItem> menus = new ArrayList<MenuItem>();
    	MenuItem menuItem = new MenuItem();
    	menuItem.setKey("accounts");
    	menuItem.setName("Hesaplar");
    	menuItem.setParentMenu("current");
    	menus.add(menuItem);
    	
    	menuItem = new MenuItem();
    	menuItem.setKey("addCurrent");
    	menuItem.setName("Cari Kart Ekleme");
    	menuItem.setParentMenu("accounts");
    	menus.add(menuItem);
    	
    	menuItem = new MenuItem();
    	menuItem.setKey("currentTransactions");
    	menuItem.setName("Cari İşlemler");
    	menuItem.setParentMenu("current");
    	menus.add(menuItem);

    	menuItem = new MenuItem();
    	menuItem.setKey("cariAlacakFisi");
    	menuItem.setName("Cari Alacak Fişi");
    	menuItem.setParentMenu("currentTransactions");
    	menus.add(menuItem);

    	menuItem = new MenuItem();
    	menuItem.setKey("cariBorcFisi");
    	menuItem.setName("Cari Borc Fişi");
    	menuItem.setParentMenu("currentTransactions");
    	menus.add(menuItem);

        return menus;
    }

}
