package com.turquaz.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
public class CurrentCard {

	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;

	private String name;
	private String currentCode;
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
    @XmlElement
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

    @XmlElement
	public String getCurrentCode() {
		return currentCode;
	}
	
	public void setCurrentCode(String currentCode) {
		this.currentCode = currentCode;
	}
	
}
